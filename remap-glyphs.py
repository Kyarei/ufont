#!/usr/bin/env python3

import argparse
import re

encoding_re = re.compile('Encoding:\s+(\d+)\s+(\d+)\s+(\d+)')

parser = argparse.ArgumentParser(description='Remap glyphs from an sfd file.')
parser.add_argument('file',
                    help='the sfd file')
parser.add_argument('src_start', metavar='src-start', type=int,
                    help='the first codepoint to remap')
parser.add_argument('src_end', metavar='src-end', type=int,
                    help='the last codepoint to remap')
parser.add_argument('dest_start', metavar='dest-start', type=int,
                    help='where the first codepoint should be remapped to')

args = parser.parse_args()

with open(args.file) as fh:
    for line in fh:
        line = line.strip()
        match = re.fullmatch(encoding_re, line)
        if not match: # Not what we're looking for
            print(line)
            continue
        a = int(match.group(1))
        b = int(match.group(2))
        c = int(match.group(3))
        if args.src_start <= a <= args.src_end:
            new_a = a - args.src_start + args.dest_start
            print(f"Encoding: {new_a} {new_a} {c}")
        else:
            print(line)
